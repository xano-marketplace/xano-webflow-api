// Get Item from List view using Search Parameter
var myUrl = new URL(document.location.href);
var myParam = myUrl.searchParams.get("id") || 1;


// Load API Endpoints from Xano to get the individual item and associated comments
let xanoUrl = new URL('https://x715-fe9c-6426.n7.xano.io/api:FFH5FMYr/item/' + myParam); // REPLACE WITH YOUR API ENDPOINT
let commentUrl = new URL('https://x715-fe9c-6426.n7.xano.io/api:FFH5FMYr/comment_by_item/' + myParam); // REPLACE WITH YOUR COMMENT API ENDPOINT

function getItem() {
    // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest()


    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', xanoUrl.toString(), true)

    request.onload = function() {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {

            var elms = document.querySelectorAll("[id='item-id']");
            for(var i = 0; i < elms.length; i++) 
            elms[i].value = data.id // <-- whatever you need to do here.

            // document.getElementById("item-id").value = data.id
            // Map a variable called itemContainer to the Webflow element called "Item-Container"
            const itemContainer = document.getElementById("Item-Container")

            // For each restaurant, create a div called card and style with the "Sample Card" class
            const item = document.getElementById('samplestyle')

            const img = item.getElementsByTagName('IMG')[0]
            img.src = data.banner.url + "?tpl=big:box";
            

            // Create an h1 and set the text content to the film's title
            const h3 = item.getElementsByTagName('H3')[0]
            h3.textContent = data.name;

            // Create a p and set the text content to the film's description
            const p = item.getElementsByTagName('P')[0]
            p.textContent = `${data.description.substring(0, 240)}` // Limit to 240 chars

            // Append the card to the div with "Item-Container" id
            itemContainer.appendChild(item);


        } else {
            console.log('error')
        }
    }

    // Send request
    request.send()

}

function getComments() {

    // Create a request variable and assign a new XMLHttpRequest object to it.
    // XMLHttpRequest is the standard way you access an API in plain Javascript.
    let request = new XMLHttpRequest();

    // Define a function (set of operations) to get item information.
    // Creates a variable that will take the URL from above and makes sure it displays as a string. 
    // We then add the word 'item" so the API endpoint becomes https://x715-fe9c-6426.n7.xano.io/api:Iw1iInWB/item
    let url = commentUrl.toString();


    // Remember the 'request' was defined above as the standard way to access an API in Javascript.
    // GET is the verb we're using to GET data from Xano
    request.open('GET', url, true)

    // When the 'request' or API request loads, do the following...
    request.onload = function() {

        // Store what we get back from the Xano API as a variable called 'data' and converts it to a javascript object
        let data = JSON.parse(this.response)

        // Status 200 = Success. Status 400 = Problem.  This says if it's successful and no problems, then execute 
        if (request.status >= 200 && request.status < 400) {

            // Map a variable called cardContainer to the Webflow element called "Cards-Container"
            const cardContainer = document.getElementById("Comment-Container")

            // This is called a For Loop. This goes through each object being passed back from the Xano API and does something.
            // Specifically, it says "For every element in Data (response from API), call each individual item item"
            data.forEach(item => {


            // For each item, create a div called card and style with the "Sample Card" class
            const style = document.getElementById('commentstyle')
            // Copy the card and it's style
            const card = style.cloneNode(true)

            card.setAttribute('id', '');
            card.style.display = 'block';

            // For each item, Create an image and use the item image coming from the API
            // const img = card.getElementsByTagName('IMG')[0]
            // img.src = item.banner.url + "?tpl=big:box"; // using Xano's template engine to re-size the pictures down and make them a box

            // For each item, create an h3 and set the text content to the item's title
            const h4 = card.getElementsByTagName('H4')[0]
            h4.textContent = item.member_name;

            // For each item, create an paragraph and set the text content to the item's description
            const p = card.getElementsByTagName('P')[0]
            p.textContent = item.comment // Limit to 240 chars

            // Place the card into the div "Cards-Container" 

            cardContainer.appendChild(card);

            })
        }
    }

    // Send item request to API
    request.send();

}

// This fires all of the defined functions when the document is "ready" or loaded
(function() {
    setTimeout(() => {
getItem();
getComments();
}, 250)
})();


