<script>
// Create a request variable and assign a new XMLHttpRequest object to it.
// XMLHttpRequest is the standard way you access an API in plain Javascript.
let request = new XMLHttpRequest();
let xanoUrl = new URL('https://x9qk-rkcz-tbuf.n7.xano.io/api:liiBipJ5/comments'); // Base URL for your Xano APIs
// Getting the post slug
const postURL = window.location.href;
let postSlug = "?slug=" + postURL.split("/").pop()
function getComments() { // Define a function (set of operations) to get comment information.
// Creates a variable that will take the URL from above and makes sure it displays as a string. 
// Add the slug of the current post to the request URL
let url = xanoUrl.toString() + postSlug;
// Remember the 'request' was defined above as the standard way to access an API in Javascript.
// GET is the verb we're using to GET data from Xano
request.open('GET', url, true)
// When the 'request' or API request loads, do the following...
request.onload = function() {
    // Store what we get back from the Xano API as a variable called 'data' and converts it to a javascript object
    let data = JSON.parse(this.response)
    // Status 200 = Success. Status 400 = Problem.  This says if it's successful and no problems, then execute 
    if (request.status >= 200 && request.status < 400) {
        // Map a variable called cardContainer to the Webflow element called "grid-comments"
        const cardContainer = document.getElementById("grid-comments")
        // This is called a For Loop. This goes through each object being passed back from the Xano API and does something.
        // Specifically, it says "For every element in Data (response from API), call each individual item comment"
        data.forEach(comments_id => {
            // For each comment, create a div called card and style with the "Sample Card" class
            const style = document.getElementById('samplestyle')
            // Copy the card and it's style
            const card = style.cloneNode(true)
            card.setAttribute('id', '');
            card.style.display = 'block';
            // For each comment, create an h3 and set the text content to the comment's title
            const h3 = card.getElementsByTagName('H3')[0]
            h3.textContent = comments_id.author;
            // For each comment, create an paragraph and set the text content to the comment's description
            const p = card.getElementsByTagName('P')[0]
            p.textContent = `${comments_id.comment}` // Limit to 240 chars
            // For each comment, create a tet block and set the content to the date / time of the comment
            const t = card.getElementsByTagName('H5')[0]
            t.textContent = `${comments_id.created_at}`
            // Place the card into the div "grid-comments" 
            cardContainer.appendChild(card);
        })
    }
}

// Send comment request to API
request.send();
}

// This fires all of the defined functions when the document is "ready" or loaded
(function() {
getComments();
})();

var Webflow = Webflow || [];
Webflow.push(function() {
  // unbind webflow form handling (keep this if you only want to affect specific forms)
  $(document).off('submit');
  /* Any form on the page */
  $('form').submit(function(e) {
    e.preventDefault();
    // https://x9qk-rkcz-tbuf.n7.xano.io/api:a-oM1laR/add_new_comment
  	const $form = $(this); // The submitted form
    const $submit = $('[type=submit]', $form); // Submit button of form
    const buttonText = $submit.val(); // Original button text
    const buttonWaitingText = $submit.attr('data-wait'); // Waiting button text value
    const formMethod = $form.attr('method'); // Form method (GET/POST)
    const formAction = $form.attr('action'); // Form action (your Xano endpoint URL)
    const formRedirect = $form.attr('data-redirect'); // Form redirect location
    // if we're editing
    // const xanoID = document.getElementById('id').value // getting the ID of the record to edit
    let requestURL = formAction.concat(postSlug + "&")
    
    let formData = new FormData;
    
    $form.serializeArray().forEach(item => {
    	formData.append(item.name, item.value);
    });
    // Set waiting text
    if (buttonWaitingText) {
      $submit.val(buttonWaitingText);
    }

    
    $.ajax(requestURL, {
    	enctype: 'multipart/form-data',
    	data: formData,
      method: formMethod,
      cache: false,
      contentType: false,
      processData: false
    })
    
    .done((res) => {
      // If form redirect setting set, then use this and prevent any other actions
      if (formRedirect) { window.location = formRedirect; return; }
      
    })
    
    .fail((res) => {
      $form
      	.siblings('.w-form-done').hide() // Hide success
    	.siblings('.w-form-fail').show(); // show failure
    })
    .always(() => {
      // Reset text
      $submit.val(buttonText);
    });
    
    // document.getElementById('grid-comments').remove(); // remove comments for refresh
    function removeComments(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
    (function() {
        const container = document.getElementById("grid-comments")
        console.log('Unloading comments...')
        removeComments(container);
        console.log('Refreshing comments...')
        let now = Date.now(),
        end = now + 300;
        while (now < end) { now = Date.now(); }
        getComments();
        })();
  });
});
</script>
