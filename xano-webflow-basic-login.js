<script>

function SendToXano() {
		event.preventDefault();
		const xano_input = 
    {
        email: document.getElementById('email').value,
        password: document.getElementById('password').value
        // You can add other inputs here if you want
    };
    fetch("YOUR XANO LOGIN ENDPOINT HERE", {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify(xano_input),
        })
				// handle response
        .then(res => res.json())
        .then(json => {
        	const xanoResponse = json;
        	console.log(xanoResponse);
        	const hasKey = Object.keys(xanoResponse).includes("authToken");
          console.log(hasKey);
          if (hasKey === false) {
          	alert("Invalid username or password.")}
            else {
            	alert("We have an authToken!");
              const authToken = xanoResponse.authToken;
              localStorage.setItem('authToken', authToken);
              location.href = "WHERE TO REDIRECT THE USER AFTER LOGIN";
              };}
        );
}
</script>
